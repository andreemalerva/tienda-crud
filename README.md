<div>
    <h1>TIENDA - CRUD</h1>
</div>

# Acerca de mí
¡Hola!

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](http://www.andreemalerva.com/), actualmente soy Desarrollador Front End, y me agrada seguir aprendiendo.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 andreemalerva@gmail.com
📲 2283530727
```

# Acerca del proyecto

Este sitio esa hecho con la ayuda de buenas tecnologías como lo son HTML, CSS y Javascript, complementando con Codeigniter, Mysql y claro Bootstrap 🫶🏻
Puedes visualizarlo en la siguiente liga: [TIENDA -CRUD for Andree Malerva😎](https://andreemalerva.com/repositorio/tienda-crud/)

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de © 2023 LYAM ANDREE CRUZ MALERVA

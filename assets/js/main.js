$(".deleteButton").each(function() {
    var href = $(this).attr('href');
    $(this).attr('href', 'javascript:void(0)');
    $(this).click(function() {
        $("#modalMsg").modal("show");
        var msg = document.getElementById('info');
        var del = document.getElementById('buttonDeleteConfirm');
        msg.innerHTML = '¿Seguro desea eliminar este producto?';
        del.setAttribute('href', href);
    });
});

$(".upProduct").each(function() {
    var href = $(this).attr('href');
    $(this).attr('href', 'javascript:void(0)');
    $(this).click(function() {
        $("#updateProductos").modal("show");
        var form = document.getElementById('formUpInfo');
        var idProduct = document.getElementById('idProduct');
        var productoUp = document.getElementById('productoUp');
        var proveedorUp = document.getElementById('proveedorUp');
        var precioUp = document.getElementById('precioUp');

        form.setAttribute('action', href);
        var buttonDeleteId = $(this).attr('id');

        $(".idProducto").each(function() {
            var idFila = $(this).attr('id');
            if (buttonDeleteId === idFila) {
                var idProductos = document.querySelector(".idProducto.id" + idFila);
                var contenido = idProductos.innerText;
                idProduct.innerHTML = contenido;
            }
        });

        $(".proveedor").each(function() {
            var idFila = $(this).attr('id');
            if (buttonDeleteId === idFila) {
                var proveedor = document.querySelector(".proveedor.id" + idFila);
                console.log('proveedor', proveedor);
                var contenido = proveedor.innerText;
                proveedorUp.setAttribute('value', contenido);
            }
        });
        $(".producto").each(function() {
            var idFila = $(this).attr('id');
            if (buttonDeleteId === idFila) {
                var producto = document.querySelector(".producto.id" + idFila);
                var contenido = producto.innerText;
                productoUp.setAttribute('value', contenido);
            }
        });
        $(".precio").each(function() {
            var idFila = $(this).attr('id');
            if (buttonDeleteId === idFila) {
                var precio = document.querySelector(".precio.id" + idFila);
                var contenido = precio.innerText;
                precioUp.setAttribute('value', contenido);
            }
        });
    });
});
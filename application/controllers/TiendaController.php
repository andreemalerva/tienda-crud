<?php
defined('BASEPATH') or exit('No direct script access allowed');
class TiendaController extends CI_Controller
{
	public $ModeloActividad;
	public $session;
	public function __construct() {
		parent::__construct();
	 }
	public function index()
	{
		$this->load->model('ModeloActividad');
		$ModeloActividad = "ModeloActividad";
		$datos['productos'] = $this->$ModeloActividad->obtenerProductos();
		$this->load->view('tienda', $datos);
	}

	public function newInfo(){
		$data = array(
			'precio' => $this->input->post('precio'),
			'producto' => $this->input->post('producto'),
			'proveedor' => $this->input->post('proveedor')
		);
		$this->load->model('ModeloActividad');
		$ModeloActividad = "ModeloActividad";
		$add = $this->ModeloActividad->saveInfoModel($data);
		redirect('/');
	 }

	 public function deleteInfo($id){
		$this->load->model('ModeloActividad');
		$this->ModeloActividad->deleteInfoModel($id);
		redirect('/');
	 }

	 public function updateInfo($id){
		$data = array(
			'precio' => $this->input->post('precio'),
			'producto' => $this->input->post('producto'),
			'proveedor' => $this->input->post('proveedor')
		);
		$this->load->model('ModeloActividad');
		$ModeloActividad = "ModeloActividad";
		$add=$this->ModeloActividad->updateInfoModel($id, $data);
		redirect('/');

	 }
}

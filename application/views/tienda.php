<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="https://img.icons8.com/ios-filled/50/connection-sync.png" sizes="16x16">
    <title>Tienda</title>
    <link rel="stylesheet" href="<?= base_url("assets/css/main.css"); ?>">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
</head>

<body>
    <div class="text-center">
        <h1>"La tienda"</h1>
        <div class="text-center p-4">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newInfo">
                Añade Productos
            </button>
        </div>
    </div>

    <div class="container">
        <table class="table table-striped table-dark">
            <?php if (count($productos)) : ?>
                <thead>
                    <tr>
                        <?php
                        foreach ($productos[0] as $clave => $fila) {
                            echo "<th scope='col'>" . $clave . "</th>";
                        }
                        ?>
                    </tr>

                </thead>
                <tbody>
                    <?php foreach ($productos as $item) : ?>
                        <tr>
                            <td class="text-center datos idProducto id<?php echo $item->id ?>" id="<?php echo $item->id ?>"> <?php echo $item->id ?> </td>
                            <td class="text-center datos precio id<?php echo $item->id ?>" id="<?php echo $item->id ?>"> <?php echo $item->precio ?> </td>
                            <td class="text-center datos producto id<?php echo $item->id ?>" id="<?php echo $item->id ?>"> <?php echo $item->producto ?> </td>
                            <td class="text-center datos proveedor id<?php echo $item->id ?>" id="<?php echo $item->id ?>"> <?php echo $item->proveedor ?> </td>
                            <td>
                                <div class='row d-flex justify-content-center flex-column px-3'>
                                    <a type='button' id="<?php echo $item->id ?>" class='btn btn-success upProduct <?php echo $item->id ?>' href="<?php echo base_url() ?>TiendaController/updateInfo/<?php echo $item->id ?>">Actualizar</a>
                                    <a type='button' class='btn btn-danger mt-3 deleteButton' href="<?php echo base_url() ?>TiendaController/deleteInfo/<?php echo $item->id ?>">Eliminar</a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            <?php else : ?>
                <p> No hay Datos </p>
            <?php endif; ?>
        </table>
        <!-- Modal Nuevo Producto-->
        <div class="modal fade" id="newInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nuevo Producto</h5>
                        <button type="button" id="cierraBtnUsuario" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="error"></div>
                        <div>
                            <form action="<?= base_url("TiendaController/newInfo"); ?>" method="POST">
                                <div class="row">
                                    <div class="col-2">
                                        <label for="producto">Producto:</label>
                                    </div>
                                    <div class="col-10">
                                        <input type="text" class="w-100" name="producto" id="producto">
                                    </div>
                                    <div class="col-2 mt-2">
                                        <label for="precio">Precio:</label>
                                    </div>
                                    <div class="col-10 mt-2" id="valor-precio">
                                        <input type="text" name="precio" class="w-100" id="precio">
                                    </div>

                                    <div class="col-2 mt-2">
                                        <label for="proveedor">Proveedor:</label>
                                    </div>
                                    <div class="col-10 mt-2">
                                        <input type="tel" class="w-100" name="proveedor" id="proveedor">
                                    </div>
                                    <div id="fecha3" class="col-10 mt-2"></div>

                                    <div class="col-12 mt-4 d-flex justify-content-end">
                                        <button type="submit" value="Guardar" class="btn btn-success" id="enviar">ENVIAR</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Actualizar Productos-->
        <div class="modal fade" id="updateProductos" tabindex="-1" role="dialog" aria-labelledby="updateProductosLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="updateProductosLabel">Actualizar Producto</h5>
                        <button type="button" id="cierraBtn" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="error"></div>
                        <div>
                            <form method="post" id="formUpInfo">
                                <div class="row">
                                    <div class="col-2">
                                        <label for="id">Id:</label>
                                    </div>
                                    <div class="col-10">
                                        <label id="idProduct">Id:</label>
                                    </div>
                                    <div class="col-2">
                                        <label for="producto">Producto:</label>
                                    </div>
                                    <div class="col-10">
                                        <input type="text" class="w-100" name="producto" id="productoUp">
                                    </div>
                                    <div class="col-2 mt-2">
                                        <label for="proveedor">Proveedor:</label>
                                    </div>
                                    <div class="col-10 mt-2">
                                        <input type="text" class="w-100" name="proveedor" id="proveedorUp">
                                    </div>
                                    <div class="col-2 mt-2">
                                        <label for="precio">Precio:</label>
                                    </div>
                                    <div class="col-10 mt-2">
                                        <input type="tel" class="w-100" name="precio" id="precioUp">
                                    </div>
                                    <div id="fecha3" class="col-10 mt-2">

                                    </div>

                                    <div class="col-12 mt-4 d-flex justify-content-end">
                                        <button type="submit" class="btn btn-info" id="botonActualizar">ENVIAR</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="container-loader d-none" id="loader-page">
                            <div class="loader"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Modal msg-->
        <div class="modal fade" id="modalMsg" tabindex="-1" role="dialog" aria-labelledby="modalMsg" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalMsgLabel">¡Mensajes de cuidado!</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p id="info"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a id="buttonDeleteConfirm" type="button" class="btn btn-danger deleteConfirm">Eliminar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="<?= base_url("assets/js/main.js"); ?>"></script>
    
</body>

</html>